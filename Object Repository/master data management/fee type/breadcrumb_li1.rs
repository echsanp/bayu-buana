<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>breadcrumb_li1</name>
   <tag></tag>
   <elementGuidId>e82d26b8-916d-40f2-9dd8-b52fbdb634f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.breadcrumb > li:nth-of-type(1)</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
