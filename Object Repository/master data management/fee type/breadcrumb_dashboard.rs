<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>breadcrumb_dashboard</name>
   <tag></tag>
   <elementGuidId>3891ac16-394f-4e04-86ec-f550c7e30624</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ol[@class='breadcrumb' and contains(.,'Fee Type')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
